export default class Person {
  constructor(name, age, description, educations) {
    this.name = name;
    this.age = age;
    this.description = description;
    this.educations = educations;
  }
}
