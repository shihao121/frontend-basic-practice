import Education from './Education';
import Person from './Person';
// eslint-disable-next-line no-undef
const $ = require('jquery');

function fetchData(url) {
  return fetch(url)
    .then(response => response.json())
    .catch(error => error);
}

const URL = 'http://localhost:3000/person';
const education = new Education();
const person = new Person();
fetchData(URL)
  .then(result => {
    person.name = result.name;
    $('.description').html(result.description);
    $('.name-age-info').html(
      `MY NAME IS ${result.name} ${result.age}YO AND THIS IN MU RESUME/CV`
    );
    $.each(result.educations, (index, data) => {
      $('tbody').append(`<tr>
                    <td rowspan="2" class="year"><b>${data.year}</b></td>
                    <td class="title"><b>${data.title}</b></td>
                </tr>
                <tr>
                    <td class="child-description">${data.description}</td>
                </tr>`);
    });
  })
  .catch(error => error);

console.log(person.name);
